export class DateTimeUtil {
  static getShortTimeString(date: Date): string {
    return [DateTimeUtil.getDoubleDigit(date.getHours()), DateTimeUtil.getDoubleDigit(date.getMinutes())].join(':');
  }

  static getTimeString(date: Date): string {
    return [
      DateTimeUtil.getDoubleDigit(date.getHours()),
      DateTimeUtil.getDoubleDigit(date.getMinutes()),
      DateTimeUtil.getDoubleDigit(date.getSeconds())
    ].join(':');
  }

  static getDateString(date: Date): string {
    return [
      DateTimeUtil.getDayOfWeek(date.getDay()),
      date.getDate(),
      DateTimeUtil.getMonth(date.getMonth()),
      date.getFullYear()
    ].join(' ');
  }

  private static getDayOfWeek(dayNumber: number): string {
    switch (dayNumber) {
      case 0:
        return 'Zondag';
      case 1:
        return 'Maandag';
      case 2:
        return 'Dinsdag';
      case 3:
        return 'Woensdag';
      case 4:
        return 'Donderdag';
      case 5:
        return 'Vrijdag';
      case 6:
        return 'Zaterdag';
    }
  }

  private static getMonth(monthNumber: number): string {
    switch (monthNumber) {
      case 0:
        return 'januari';
      case 1:
        return 'februari';
      case 2:
        return 'maart';
      case 3:
        return 'april';
      case 4:
        return 'mei';
      case 5:
        return 'juni';
      case 6:
        return 'juli';
      case 7:
        return 'augustus';
      case 8:
        return 'september';
      case 9:
        return 'oktober';
      case 10:
        return 'november';
      case 11:
        return 'december';
    }
  }

  private static getDoubleDigit(digit: number): string {
    return (digit < 10 ? '0' : '') + digit.toString();
  }
}
