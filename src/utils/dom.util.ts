export class DomUtil {
  static removeChildren(element: HTMLElement): void {
    while (element.hasChildNodes()) {
      element.removeChild(element.lastChild);
    }
  }
}
