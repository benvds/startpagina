export class IntervalUtil {
  static handleInterval(action: (...args: any[]) => void, timeout: number, oldIntervalId?: number): number {
    if (oldIntervalId) {
      IntervalUtil.stopInterval(oldIntervalId);
    }
    return window.setInterval(action, timeout);
  }

  static stopInterval(intervalId: number): void {
    window.clearInterval(intervalId);
  }
}
