import { Singleton } from 'typescript-ioc/es6';

@Singleton
export class HttpService {
  async get(url: string): Promise<Response> {
    const options = {
      cache: 'default',
      method: 'GET'
    } as RequestInit;
    return await fetch(url, options);
  }
}
