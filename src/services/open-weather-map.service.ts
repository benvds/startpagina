import { Inject, Singleton } from 'typescript-ioc/es6';
import { ICurrentWeather } from '../model/current-weather.model';
import { IForecast } from '../model/forecast.model';
import { HttpService } from './http.service';
import { LocalStorageService } from './local-storage.service';

@Singleton
export class OpenWeatherMapService {
  static ERROR_APIKEY_INVALID = 'OpenWeatherMap authenticatie ongeldig';
  static ERROR_APIKEY_EMPTY = 'OpenWeatherMap key is leeg';
  static ERROR_LOCATION = 'Locatie niet gevonden';

  private static apiKeyLocalStorageId = 'openWeatherMapApiKey';
  private static url = 'https://api.openweathermap.org/data/2.5/';

  private apiKey: string;
  @Inject
  private http: HttpService;
  @Inject
  private localStorage: LocalStorageService;

  setApiKey(apiKey: string): void {
    this.apiKey = apiKey;
    this.localStorage.setItem(OpenWeatherMapService.apiKeyLocalStorageId, this.apiKey);
  }

  async getCurrentWeather(city: string): Promise<ICurrentWeather> {
    this.checkApiKey();
    const weatherUrl = [
      OpenWeatherMapService.url,
      'weather?q=',
      encodeURIComponent(city),
      '&units=metric&lang=nl&APPID=',
      this.apiKey
    ].join('');

    const response = await this.http.get(weatherUrl);
    return this.handleResponse<ICurrentWeather>(response);
  }

  async getFiveDayForecast(city: string): Promise<IForecast> {
    this.checkApiKey();
    const weatherUrl = [
      OpenWeatherMapService.url,
      'forecast?q=',
      encodeURIComponent(city),
      '&units=metric&lang=nl&APPID=',
      this.apiKey
    ].join('');

    const response = await this.http.get(weatherUrl);
    return this.handleResponse<IForecast>(response);
  }

  private hasApiKey(): boolean {
    if (!this.apiKey) {
      this.apiKey = this.localStorage.getItem(OpenWeatherMapService.apiKeyLocalStorageId);
    }
    return !!this.apiKey;
  }

  private checkApiKey(): void {
    if (!this.hasApiKey()) {
      throw Error(OpenWeatherMapService.ERROR_APIKEY_EMPTY);
    }
  }

  private handleResponse<T>(response: Response) {
    switch (response.status) {
      case 200:
        return response.json() as Promise<T>;
      case 401:
        throw Error(OpenWeatherMapService.ERROR_APIKEY_INVALID);
      case 404:
        throw Error(OpenWeatherMapService.ERROR_LOCATION);
    }
  }
}
