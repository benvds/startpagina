import { Singleton } from 'typescript-ioc/es6';
import { SearchEngine } from '../components/search.component';

@Singleton
export class SearchService {
  static getSearchEngineUrl(zoekterm: string, engine: SearchEngine = SearchEngine.DuckDuckGo): string {
    zoekterm = encodeURIComponent(zoekterm);
    switch (engine) {
      case SearchEngine.Hoelangkoken:
        return `https://hoelangkoken.nl/?s=${zoekterm}`;
      case SearchEngine.Google:
        return `https://google.com/search?q=${zoekterm}`;
      case SearchEngine.DuckDuckGo:
      default:
        return `https://duckduckgo.com/?q=${zoekterm}`;
    }
  }
}
