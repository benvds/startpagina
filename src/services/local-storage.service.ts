import { Singleton } from 'typescript-ioc/es6';

@Singleton
export class LocalStorageService {
  getItem<T>(id: string): T {
    return JSON.parse(localStorage.getItem(id)) as T;
  }

  setItem<T>(id: string, object: T): void {
    localStorage.setItem(id, JSON.stringify(object));
  }

  removeItem(id: string): void {
    localStorage.removeItem(id);
  }
}
