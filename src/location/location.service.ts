import { Inject, Singleton } from 'typescript-ioc/es6';
import { LocalStorageService } from '../services/local-storage.service';

@Singleton
export class LocationService {
  private static locationStorageId = 'location';
  private static defaultLocation = 'Deventer';
  private location: string;

  constructor(@Inject private storageService: LocalStorageService) {}

  getLocation(): string {
    if (this.location) {
      return this.location;
    }

    let location = this.storageService.getItem<string>(LocationService.locationStorageId);
    if (!location) {
      location = LocationService.defaultLocation;
      this.storageService.setItem(LocationService.locationStorageId, location);
    }

    this.location = location;
    return location;
  }

  setLocation(location: string): void {
    this.location = location;
    this.storageService.setItem(LocationService.locationStorageId, location);
  }
}
