import { Inject } from 'typescript-ioc/es6';
import { LocationService } from './location.service';

export class LocationComponent {
  static setLocation(location: string) {
    LocationComponent.getLocationElement().innerText = location;
  }

  private static getLocationElement(): HTMLElement {
    return document.getElementById('location');
  }

  @Inject
  locationService: LocationService;

  constructor() {
    LocationComponent.setLocation(this.locationService.getLocation());
    LocationComponent.getLocationElement().onclick = () => this.changeLocation();
  }

  changeLocation() {
    const location = prompt('Voer je locatie in', this.locationService.getLocation());
    if (location) {
      this.locationService.setLocation(location);
      LocationComponent.setLocation(location);
      // TODO weerinformatie opnieuw laten ophalen bij wijzigen van locatie
      // WeatherComponent.handleCurrentWeather();
    }
  }
}
