import * as Mustache from 'mustache';
import { Inject } from 'typescript-ioc/es6';
import { Shortcut } from './shortcut.model';
import { ShortcutService } from './shortcut.service';

export class ShortcutComponent {
  @Inject
  private shortcutService: ShortcutService;

  constructor() {
    this.renderShortcuts();
    this.initSettings();
  }

  private initSettings(): void {
    document.getElementById('addShortcutButton').onclick = () => {
      this.openAddShortcutPopup();
    };
  }

  private openAddShortcutPopup() {
    const contentContainer = document.getElementById('contentContainer');
    contentContainer.classList.add('isBlurred');
    document.getElementById('popupContainer').classList.remove('isHidden');
    const shortcutSelectOptionsTemplate = document.getElementById('shortcutSelectOptionsTemplate');
    const shortcutSelectOptionTemplate = document.getElementById('shortcutSelectOptionTemplate');
    document.getElementById('shortcutParent').innerHTML = Mustache.render(
      shortcutSelectOptionsTemplate.innerHTML,
      { shortcuts: this.shortcutService.getShortcuts() },
      {
        shortcutSelectOption: shortcutSelectOptionTemplate.innerHTML
      }
    );
    document.getElementById('submitAddShortcutPopup').onclick = () => this.submitAddShortcutPopup();
    for (const element of document.getElementsByClassName('closeAddShortcutPopup')) {
      if (element instanceof HTMLButtonElement) {
        element.onclick = () => this.closeAddShortcutPopup();
      }
    }
  }

  private submitAddShortcutPopup() {
    const nameInput = document.getElementById('shortcutNaam') as HTMLInputElement;
    const urlInput = document.getElementById('shortcutUrl') as HTMLInputElement;
    if (this.shortcutService.getShortcutByUrl(urlInput.value)) {
      throw Error('Er bestaat al een snelkoppeling met deze url');
    }
    const newShortcut = new Shortcut(nameInput.value, urlInput.value);
    const parentSelect = document.getElementById('shortcutParent') as HTMLSelectElement;
    const selectedParentUrl = parentSelect.selectedOptions[0].value;
    const parent = selectedParentUrl ? this.shortcutService.getShortcutByUrl(selectedParentUrl) : null;
    this.shortcutService.addShortcut(newShortcut, parent);
    this.renderShortcuts();
    this.closeAddShortcutPopup();

    nameInput.value = '';
    urlInput.value = '';
    parentSelect.selectedIndex = 0;
  }

  private closeAddShortcutPopup() {
    document.getElementById('contentContainer').classList.remove('isBlurred');
    document.getElementById('popupContainer').classList.add('isHidden');
  }

  private renderShortcuts(): void {
    const shortcuts = this.shortcutService.getShortcuts();
    const shortcutContainerElement = document.getElementById('shortcutContainer');
    const shortcutContainerTemplate = document.getElementById('shortcutContainerTemplate');
    const shortcutTemplate = document.getElementById('shortcutTemplate');
    shortcutContainerElement.innerHTML = Mustache.render(
      shortcutContainerTemplate.innerHTML,
      { shortcuts },
      { shortcut: shortcutTemplate.innerHTML }
    );
  }
}
