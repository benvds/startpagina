export interface IShortcut {
  url: string;
  displayName: string;
  children: Shortcut[];
}

export class Shortcut implements IShortcut {
  url: string;
  displayName: string;
  children: Shortcut[];

  constructor(displayName: string = null, url: string, children: Shortcut[] = []) {
    this.url = url;
    this.displayName = displayName;
    this.children = children;
  }
}
