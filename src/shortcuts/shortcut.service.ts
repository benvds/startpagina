import { Inject, Singleton } from 'typescript-ioc/es6';
import { LocalStorageService } from '../services/local-storage.service';
import { IShortcut, Shortcut } from './shortcut.model';

@Singleton
export class ShortcutService {
    private static storageId = 'shortcuts';

    private static sortChildren(shortcut: IShortcut, recursive = false): void {
        shortcut.children.sort((s1, s2) => ShortcutService.shortcutCompare(s1, s2));
        if (recursive) {
            for (const child of shortcut.children) {
                ShortcutService.sortChildren(child, recursive);
            }
        }
    }

    private static shortcutCompare(shortcut1: IShortcut, shortcut2: IShortcut): number {
        if (shortcut1.displayName > shortcut2.displayName) {
            return 1;
        }
        if (shortcut1.displayName < shortcut2.displayName) {
            return -1;
        }
        return 0;
    }

    private shortcuts: IShortcut[];
    constructor(@Inject private storageService: LocalStorageService) { }

    getShortcuts(): IShortcut[] {
        if (!this.shortcuts) {
            this.getFromLocalStorage();
        }
        return this.shortcuts;
    }

    getShortcutByUrl(url: string): IShortcut {
        if (!this.shortcuts) {
            this.getFromLocalStorage();
        }

        return this.findShortcutByUrl(url, this.shortcuts);
    }

    addShortcut(shortcut: IShortcut, parent: IShortcut = null): void {
        if (!parent) {
            this.shortcuts.push(shortcut);
            this.sortShortcuts();
        } else {
            parent.children.push(shortcut);
            ShortcutService.sortChildren(parent);
        }
        this.storageService.setItem(ShortcutService.storageId, this.shortcuts);
    }

    sortAllShortcuts(): void {
        this.sortShortcuts();
        for (const shortcut of this.shortcuts) {
            ShortcutService.sortChildren(shortcut, true);
        }
    }

    private findShortcutByUrl(url: string, shortcuts: IShortcut[]): IShortcut {
        for (const shortcut of shortcuts) {
            if (shortcut.url === url) {
                return shortcut;
            }
            const matchingChild = this.findShortcutByUrl(url, shortcut.children);
            if (matchingChild) {
                return matchingChild;
            }
        }
        return undefined;
    }

    private getFromLocalStorage() {
        const localStorageShortcuts = this.storageService.getItem<Shortcut[]>(ShortcutService.storageId);
        this.shortcuts = localStorageShortcuts ? localStorageShortcuts : [];
    }

    private sortShortcuts(): void {
        this.shortcuts.sort((s1, s2) => ShortcutService.shortcutCompare(s1, s2));
    }
}