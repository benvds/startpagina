import { DateTimeComponent } from './components/date-time.component';
import { SearchComponent } from './components/search.component';
import { WeatherComponent } from './components/weather.component';
import './index.scss';
import { ErrorInterceptor } from './interceptors/error.interceptor';
import { LocationComponent } from './location/location.component';
import { ShortcutComponent } from './shortcuts/shortcut.component';

class Main {
  constructor() {
    window.onerror = ErrorInterceptor.handleError;
    // tslint:disable-next-line:no-unused-expression
    new ShortcutComponent();
    // tslint:disable-next-line:no-unused-expression
    new SearchComponent();
    // tslint:disable-next-line:no-unused-expression
    new DateTimeComponent();
    // tslint:disable-next-line:no-unused-expression
    new LocationComponent();
    // tslint:disable-next-line:no-unused-expression
    new WeatherComponent();
  }
}

window.onload = () => new Main();
