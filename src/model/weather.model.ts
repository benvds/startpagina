import { DateTimeUtil } from '../utils/date-time.util';
import { ICurrentWeather } from './current-weather.model';

export interface IWeather {
  description: string;
  maxTemp: number;
  minTemp: number;
  sunUp: Date;
  sunUpTime: string;
  sunDown: Date;
  sunDownTime: string;
  windSpeed: number;
  windDirection: number;
  rain: number; // mm in next 3 hours
  snow: number; // mm in next 3 hours
  location: string;
  lastUpdate: Date;
  icon: string;
}

export class Weather implements IWeather {
  static fromInterface(weatherData: IWeather): Weather {
    const weather = new Weather();
    weather.description = weatherData.description;
    weather.maxTemp = weatherData.maxTemp;
    weather.minTemp = weatherData.minTemp;
    weather.sunUp = new Date(weatherData.sunUp);
    weather.sunUpTime = DateTimeUtil.getShortTimeString(weather.sunUp);
    weather.sunDown = new Date(weatherData.sunDown);
    weather.sunDownTime = DateTimeUtil.getShortTimeString(weather.sunDown);
    weather.windSpeed = weatherData.windSpeed;
    weather.windDirection = weatherData.windDirection;
    weather.rain = weatherData.rain;
    weather.snow = weatherData.snow;
    weather.location = weatherData.location;
    weather.lastUpdate = new Date(weatherData.lastUpdate);
    weather.icon = weatherData.icon;
    return weather;
  }
  static fromCurrentWeather(currentWeather: ICurrentWeather): Weather {
    const weather = new Weather();
    weather.description = currentWeather.weather[0].description;
    weather.maxTemp = currentWeather.main.temp_max;
    weather.minTemp = currentWeather.main.temp_min;
    weather.windSpeed = currentWeather.wind.speed;
    weather.windDirection = currentWeather.wind.deg;
    weather.rain = currentWeather.rain ? currentWeather.rain['3h'] : null;
    weather.snow = currentWeather.snow ? currentWeather.snow['3h'] : null;
    weather.sunUp = new Date(currentWeather.sys.sunrise * 1000);
    weather.sunUpTime = DateTimeUtil.getShortTimeString(weather.sunUp);
    weather.sunDown = new Date(currentWeather.sys.sunset * 1000);
    weather.sunDownTime = DateTimeUtil.getShortTimeString(weather.sunDown);
    weather.location = currentWeather.name;
    weather.lastUpdate = new Date(currentWeather.dt * 1000);
    weather.icon = Weather.openWeatherMapIconToUnicodeEmoje(currentWeather.weather[0].icon);
    return weather;
  }

  private static openWeatherMapIconToUnicodeEmoje(icon: string): string {
    switch (icon.substring(0, 2)) {
      case '01':
        return '☀';
      case '02':
        return '⛅';
      case '03':
      case '04':
        return '☁';
      case '09':
        return '🌧';
      case '10':
        return '🌦';
      case '11':
        return '🌩';
      case '13':
        return '❄';
      case '50':
        return '🌫';
      default:
        return '🌠';
    }
  }

  description: string;
  maxTemp: number;
  minTemp: number;
  sunUp: Date;
  sunUpTime: string;
  sunDown: Date;
  sunDownTime: string;
  windSpeed: number;
  windDirection: number;
  rain: number; // mm in next 3 hours
  snow: number; // mm in next 3 hours
  location: string;
  lastUpdate: Date;
  icon: string;
}
