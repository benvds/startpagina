import { IClouds, ICoord, IWeather, IWind } from './current-weather.model';

export interface ICity {
  id: number;
  name: string;
}

export interface IMain {
  temp: number;
  temp_min: number;
  temp_max: number;
  pressure: number;
  sea_level: number;
  grnd_level: number;
  humidity: number;
  temp_kf: number;
}

export interface ISys {
  pod: string;
}

export interface IList {
  dt: number;
  main: IMain;
  weather: IWeather[];
  clouds: IClouds;
  wind: IWind;
  sys: ISys;
  dt_txt: string;
}

export interface IForecast {
  city: ICity;
  coord: ICoord;
  country: string;
  cod: string;
  message: number;
  cnt: number;
  list: IList[];
}
