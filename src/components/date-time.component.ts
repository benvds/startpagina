import { DateTimeUtil } from '../utils/date-time.util';
import { IntervalUtil } from '../utils/interval.util';

export class DateTimeComponent {
  private dateTimeIntervalId: number;

  constructor() {
    this.setDatumTijd();
    this.dateTimeIntervalId = IntervalUtil.handleInterval(() => this.setDatumTijd(), 1000, this.dateTimeIntervalId);
  }

  private setDatumTijd(): void {
    const date = new Date();

    document.getElementById('time').innerText = DateTimeUtil.getTimeString(date);

    const dateElement = document.getElementById('date');
    const dateString = DateTimeUtil.getDateString(date);
    if (dateElement.innerText !== dateString) {
      dateElement.innerText = dateString;
    }
  }
}
