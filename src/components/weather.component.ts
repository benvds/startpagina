import * as Mustache from 'mustache';
import { Inject } from 'typescript-ioc/es6';
import { LocationComponent } from '../location/location.component';
import { LocationService } from '../location/location.service';
import { ICurrentWeather } from '../model/current-weather.model';
import { IWeather, Weather } from '../model/weather.model';
import { LocalStorageService } from '../services/local-storage.service';
import { OpenWeatherMapService } from '../services/open-weather-map.service';
import { IntervalUtil } from '../utils/interval.util';

export class WeatherComponent {
  private static currentWeatherStorageKey = 'currentWeather';
  private static currentWeatherIntervalTimeout = 1000 * 60 * 15; // milliseconds
  private static currentWeatherCacheDuration = 1000 * 60 * 60; // milliseconds

  private currentWeatherIntervalId: number;

  @Inject
  private weatherService: OpenWeatherMapService;
  @Inject
  private locationService: LocationService;
  @Inject
  private localStorage: LocalStorageService;

  constructor() {
    this.getCurrentWeather();
    this.currentWeatherIntervalId = IntervalUtil.handleInterval(
      () => this.getCurrentWeather(),
      WeatherComponent.currentWeatherIntervalTimeout,
      this.currentWeatherIntervalId
    );
  }

  async getCurrentWeather(): Promise<void> {
    const location = this.locationService.getLocation();
    const cachedWeatherData = this.localStorage.getItem<IWeather>(WeatherComponent.currentWeatherStorageKey);

    if (cachedWeatherData) {
      if (cachedWeatherData.location !== location) {
        this.localStorage.removeItem(WeatherComponent.currentWeatherStorageKey);
      } else {
        const cachedWeather = Weather.fromInterface(cachedWeatherData);
        if (Date.now() - cachedWeather.lastUpdate.getTime() < WeatherComponent.currentWeatherCacheDuration) {
          this.renderCurrentWeather(cachedWeather);
          return;
        }
      }
    }

    try {
      const newCurrentWeather = await this.weatherService.getCurrentWeather(location);
      this.handleCurrentWeather(newCurrentWeather);
    } catch (error) {
      if (!(error instanceof Error)) {
        // Er kwam iets in de catch wat geen error was, ik weet niet wat het wel is
        throw error;
      }
      this.handleOpenWeatherMapError(error);
    }
  }

  handleCurrentWeather(weatherData: ICurrentWeather) {
    const weather = Weather.fromCurrentWeather(weatherData);
    this.updateLocationIfNecessary(weather);
    this.localStorage.setItem(WeatherComponent.currentWeatherStorageKey, weather);

    this.renderCurrentWeather(weather);
  }

  private handleOpenWeatherMapError(error: Error) {
    switch (error.message) {
      case OpenWeatherMapService.ERROR_APIKEY_EMPTY:
      case OpenWeatherMapService.ERROR_APIKEY_INVALID:
        if (this.askApiKey()) {
          this.getCurrentWeather();
        } else {
          IntervalUtil.stopInterval(this.currentWeatherIntervalId);
        }
        break;
      case OpenWeatherMapService.ERROR_LOCATION:
        window.alert(
          'Locatie werd niet gevonden. ' +
            'Kijk of je het goed hebt getypt of probeer een andere locatie bij jou in de buurt.'
        );
      default:
        throw error;
    }
  }

  private updateLocationIfNecessary(weather: Weather) {
    if (weather.location !== this.locationService.getLocation()) {
      this.locationService.setLocation(weather.location);
      LocationComponent.setLocation(weather.location);
    }
  }

  private renderCurrentWeather(weather: Weather): void {
    document.getElementById('currentWeather').innerHTML = this.renderWeatherTemplate(weather, 'Nu');
  }

  private renderWeatherTemplate(weather: Weather, headerText: string): string {
    return Mustache.render(document.getElementById('weatherTemplate').innerHTML, { weather, headerText });
  }

  private askApiKey(): boolean {
    const message =
      'OpenWeatherMap apikey is leeg of niet (meer) geldig, vul een geldige apikey in.' +
      '\n\nLaat leeg of annuleer om geen weerinformatie op te halen.';
    const apiKey = prompt(message);
    this.weatherService.setApiKey(apiKey);
    return !!apiKey;
  }
}
