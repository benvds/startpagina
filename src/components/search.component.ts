import { Inject } from 'typescript-ioc/es6';
import { LocalStorageService } from '../services/local-storage.service';
import { SearchService } from '../services/search.service';
import { DomUtil } from '../utils/dom.util';

export enum SearchEngine {
  DuckDuckGo = 'DuckDuckGo',
  Google = 'Google',
  Hoelangkoken = 'Hoelangkoken'
}

export class SearchComponent {
  private selectedSearchEngine = SearchEngine.DuckDuckGo;
  @Inject
  private storageService: LocalStorageService;

  constructor() {
    this.setSearchEngine(this.getSearchEngine(), false, false);
    this.initSearchEngineSelector();
    document.getElementById('search').onkeyup = event => {
      if (event.key === 'Enter') {
        const element = event.target as HTMLInputElement;
        this.doeZoeken(element.value);
        element.value = '';
      }
    };
  }

  private doeZoeken(zoekTerm: string): void {
    if (!zoekTerm) {
      return;
    }

    const url = SearchService.getSearchEngineUrl(zoekTerm, this.selectedSearchEngine);
    window.open(url);
  }

  private initSearchEngineSelector(): void {
    const searchEngineSelectElement = document.getElementById('searchEngineSelect') as HTMLSelectElement;
    DomUtil.removeChildren(searchEngineSelectElement);
    searchEngineSelectElement.onchange = event => {
      const element = event.target as HTMLSelectElement;
      this.setSearchEngine(SearchEngine[element.value as keyof typeof SearchEngine]);
    };
    for (const engine of Object.keys(SearchEngine)) {
      const optionElement = document.createElement('option');
      optionElement.value = engine;
      optionElement.innerText = SearchEngine[engine as keyof typeof SearchEngine];
      optionElement.selected = this.selectedSearchEngine === engine;
      searchEngineSelectElement.appendChild(optionElement);
    }
  }

  private getSearchEngine(): SearchEngine {
    let engine = this.storageService.getItem<SearchEngine>('selectedSearchEngine');
    if (!engine) {
      engine = SearchEngine.DuckDuckGo;
    }
    return engine;
  }

  private setSearchEngine(engine: SearchEngine, focusSearch: boolean = true, save: boolean = true): void {
    if (!engine) {
      throw Error('De gekozen zoekmachine lijkt niet beschikbaar.');
    }

    this.selectedSearchEngine = engine;
    if (save) {
      this.storageService.setItem('selectedSearchEngine', engine);
    }
    if (focusSearch) {
      document.getElementById('search').focus();
    }
  }
}
