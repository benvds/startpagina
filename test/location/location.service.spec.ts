import * as chai from 'chai';
import sinon = require('sinon');
import { LocationService } from '../../src/location/location.service';
import { LocalStorageService } from '../../src/services/local-storage.service';

const expect = chai.expect;
let storageService: LocalStorageService;
let service: LocationService;

beforeEach(() => {
  storageService = new LocalStorageService();
  service = new LocationService(storageService);
});

describe(LocationService.name, () => {
  describe('getLocation', () => {
    it('should get the location from localstorage', () => {
      storageService.getItem = sinon.fake.returns('Zwolle');

      const result = service.getLocation();

      expect(result).to.equal('Zwolle');
    });

    it('should return the default location when localstorage is empty', () => {
      storageService.getItem = sinon.fake.returns(undefined);
      storageService.setItem = sinon.fake();

      const result = service.getLocation();

      expect(result).to.equal('Deventer');
    });
  });
  describe('setLocation', () => {
    it('should set the location in the localstorage', () => {
      const newLocation = 'Amsterdam';

      const setItemMock = sinon.mock(storageService);
      setItemMock
        .expects(storageService.setItem.name)
        .withArgs('location', newLocation)
        .once();

      service.setLocation(newLocation);

      setItemMock.verify();
    });
  });
});
