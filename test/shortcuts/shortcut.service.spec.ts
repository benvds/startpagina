import * as chai from 'chai';
import sinon = require('sinon');
import { LocalStorageService } from '../../src/services/local-storage.service';
import { IShortcut, Shortcut } from '../../src/shortcuts/shortcut.model';
import { ShortcutService } from '../../src/shortcuts/shortcut.service';

const expect = chai.expect;
let storageService: LocalStorageService;
let service: ShortcutService;

const shortcuts = [
  new Shortcut('duckduckgo', 'https://www.duckduckgo.com'),
  new Shortcut('google', 'https://www.google.com', [new Shortcut('keep', 'https://keep.google.com')])
];

beforeEach(() => {
  storageService = new LocalStorageService();
  service = new ShortcutService(storageService);
});

describe(ShortcutService.name, () => {
  describe('getShortcuts', () => {
    it('should return the shortcuts from localstorage', () => {
      fakeLocalStorageItems(shortcuts);

      const result = service.getShortcuts();

      expect(result.length).to.equal(2);
      expect(result).to.deep.equal(shortcuts);
    });
    it('should return an empty list when localstorage is empty', () => {
      fakeLocalStorageItems(undefined);

      const result = service.getShortcuts();

      expect(result.length).to.equal(0);
    });
  });

  describe('getShortcutByUrl', () => {
    it('should find a shortcut by url', () => {
      fakeLocalStorageItems(shortcuts);

      const testUrl = 'https://www.duckduckgo.com';
      const result = service.getShortcutByUrl(testUrl);

      verifyShortcut(shortcuts[0].displayName, testUrl, result);
    });
    it('should return undefined when nothing is found', () => {
      fakeLocalStorageItems(shortcuts);

      const testUrl = 'https://www.bing.com';
      const result = service.getShortcutByUrl(testUrl);

      expect(result).to.equal(undefined);
    });
    it('should also find a nested shortcut by url', () => {
      fakeLocalStorageItems(shortcuts);

      const testUrl = 'https://keep.google.com';
      const result = service.getShortcutByUrl(testUrl);

      verifyShortcut('keep', testUrl, result);
    });
  });

  describe('sortAllShortcuts', () => {
    const unsortedShortcuts = [
      new Shortcut('duckduckgo', 'https://www.duckduckgo.com'),
      new Shortcut('reddit', 'https://www.reddit.com'),
      new Shortcut('google', 'https://www.google.com')
    ];

    it('should sort shortcuts by display name', () => {
      fakeLocalStorageItems(unsortedShortcuts);
      service.getShortcuts();

      service.sortAllShortcuts();
      const result = service.getShortcuts();

      verifyShortcut('duckduckgo', 'https://www.duckduckgo.com', result[0]);
      verifyShortcut('google', 'https://www.google.com', result[1]);
      verifyShortcut('reddit', 'https://www.reddit.com', result[2]);
    });
  });
});

function verifyShortcut(displayName: string, url: string, actual: IShortcut) {
  expect(actual).to.be.ok;
  expect(actual.displayName).to.equal(displayName);
  expect(actual.url).to.equal(url);
}

function fakeLocalStorageItems(items: IShortcut[]) {
  storageService.getItem = sinon.fake.returns(items);
}
