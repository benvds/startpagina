import * as chai from 'chai';
import { DateTimeUtil } from '../../src/utils/date-time.util';

const expect = chai.expect;

const date = new Date(2018, 9, 26, 16, 34, 13); //  26 october 2018, Silly JavaScript 0-indexed month format
const dateWithSingleDigits = new Date(2008, 9, 3, 8, 5, 7);

describe(DateTimeUtil.name, () => {
  describe('getDateString', () => {
    it('should get a dutch date string', () => {
      const result = DateTimeUtil.getDateString(date);
      expect(result).to.equal('Vrijdag 26 oktober 2018');
    });
    it('should get a dutch date string without leading zeroes', () => {
      const result = DateTimeUtil.getDateString(dateWithSingleDigits);
      expect(result).to.equal('Vrijdag 3 oktober 2008');
    });
  });
  describe('getTimeString', () => {
    it('should get a time string', () => {
      const result = DateTimeUtil.getTimeString(date);
      expect(result).to.equal('16:34:13');
    });
    it('should get a time string with leading zeroes', () => {
      const result = DateTimeUtil.getTimeString(dateWithSingleDigits);
      expect(result).to.equal('08:05:07');
    });
  });
  describe('getShortTimeString', () => {
    it('should get a short time string', () => {
      const result = DateTimeUtil.getShortTimeString(date);
      expect(result).to.equal('16:34');
    });
    it('should get a short time string with leading zeroes', () => {
      const result = DateTimeUtil.getShortTimeString(dateWithSingleDigits);
      expect(result).to.equal('08:05');
    });
  });
});
