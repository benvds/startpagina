# Startpagina

[![Netlify Status](https://api.netlify.com/api/v1/badges/e058baae-615c-4ee9-a8db-9ae05445b610/deploy-status)](https://app.netlify.com/sites/vdstouwe/deploys)

https://start.famvdstouwe.nl

## Development

### Requirements

- Node.JS of Docker + Visual Studio Code (+ Remote Development extension)

### Startup

- Clone deze repository

#### Node.JS

- Open een terminal in de root directory
- `npm i`
- `npm run build:watch`

#### Docker + Visual Studio Code

- Open Visual Studio Code
- Command Palette (`ctrl + shift + p`): Open Folder in Container...
- Selecteer de root directory van de geclonede repository
- Run Task `npm: install`
- Run Task `npm: build:watch`
